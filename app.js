'use strict';

// Declare an app module that depends on views and components

angular.module('app', [
  'ngRoute',
  'app.coffees',
  'app.reviews',
  'app.version'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/coffees'});
}]);
